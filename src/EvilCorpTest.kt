import org.junit.Assert.*
import org.junit.Test

/**
 * @author Vince Feskens
 */
class EvilCorpTest {
    @Test
    fun shouldCensor() {
        //given
        val input = "You are a nice person"

        //when
        val result = EvilCorp.censor(input)

        //then
        assertEquals("You are a XXXX person", result)
    }

    @Test
    fun shouldCensor2() {
        //given
        val input = "Such a nice day with a bright sun, makes me happy"

        //when
        val result = EvilCorp.censor(input)

        //then
        assertEquals("Such a XXXX day with a bright XXX, makes me XXXXX", result)
    }

    @Test
    fun shouldCensor3() {
        //given
        val input = "You are so friendly!"

        //when
        val result = EvilCorp.censor(input)

        //then
        assertEquals("You are so XXXXXXXXX", result)
    }

    @Test
    fun shouldReplaceBadWords() {
        //given
        val input = "Objection is bad, a better thing to do, is to agree."

        //when
        val result = EvilCorp.censor(input)

        //then
        assertEquals("Thoughtcrime is ungood, a gooder thing to do, is to crimestop.", result)
    }

}