/**
 * @author Vince Feskens
 */
class EvilCorp {


    companion object {
        private val blackList = listOf("nice", "pony", "sun", "light", "fun", "happy", "funny", "joy", "friend")

        private val replaceMap = mapOf(
            Pair("bad", "ungood"),
            Pair("better", "gooder"),
            Pair("objection", "thoughtcrime"),
            Pair("agree", "crimestop")
        )

        fun censor(input: String): String {

            val wordsToReplace = input.split(" ", ",")
                .filter { word -> wordIsBlacklisted(word) }

            var result = input
            wordsToReplace
                .forEach { wordToReplace -> result = result.replace(wordToReplace, "X".repeat(wordToReplace.length)) }

            this.replaceMap.forEach { key, value ->
                result = result.replace(
                    Regex(key, RegexOption.IGNORE_CASE)
                ) { matchResult -> determineNewValue(matchResult.value, value) }
            }

            return result
        }

        private fun determineNewValue(value: String, key: String): String {
            val casing = value.map { char -> char.isUpperCase() }

            return key.mapIndexed { index, c ->
                if (casing.getOrNull(index) != null)
                    if (casing.get(index))
                        c.toUpperCase()
                    else
                        c
                else
                    c
            }.joinToString("")
        }

        private fun wordIsBlacklisted(word: String): Boolean {
            blackList.forEach { blackListWord ->
                if (word.startsWith(blackListWord))
                    return true
            }
            return false
        }
    }
}